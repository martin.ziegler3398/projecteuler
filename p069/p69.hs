import Mlib

eulerphi :: Integer -> Integer
eulerphi n = let factors = removeMult $ factorize n in foldr (\x y -> y - y `div` x) n factors

removeMult :: (Integral a) => [a] -> [a]
removeMult [] = []
removeMult (x:xs) = x : (removeMult $ dropWhile (==x) xs)

a69 :: (Ord a, Fractional a) => Integer -> (Integer, a)
a69 x = max $ map (\x -> (x,(fromIntegral x)/(fromIntegral $ eulerphi x))) [2..x]
          where max (x:[]) = x
                max ((x1, x2):(y1, y2):xs) = max $ if x2 > y2 then (x1,x2):xs else (y1,y2):xs