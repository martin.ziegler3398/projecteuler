module Mlib (primes, primesUntil, primesFirstN, isPrime, ggt, fib, factorize) where

primes :: [Integer]
primes = 2 : 3 : 5 : primes'
          where
           isPrime (p:ps) n = p*p > n || n `rem` p /= 0 && isPrime ps n
           primes' = 7 : filter (isPrime primes') (scanl (+) 11 $ cycle [2,4,2,4,6,2,6,4])

primesUntil :: Integer -> [Integer]
primesUntil n = takeWhile (\x -> x <= n) primes

primesFirstN :: Int -> [Integer]
primesFirstN n = take n primes

isPrime :: Integer -> Bool
isPrime x = x `elem` (primesUntil x)

ggt :: Int -> Int -> Int
ggt a b | a < 0 = ggt (abs a) b
        | b < 0 = ggt a (abs b)
        | a >= b = ggt b a
        | b `mod` a == 0 = a
        | otherwise = ggt a (b `mod` a)

fib :: Int -> Integer
fib = (map f [0..] !!)
      where 
        f 0 = 0
        f 1 = 1
        f n = fib (n-1) + fib (n - 2)

factorize :: Integer -> [Integer]
factorize = factor primes
             where factor ps@(p:pt) n | p*p > n      = [n]               
                                      | rem n p == 0 = p : factor ps (quot n p) 
                                      | otherwise    =     factor pt n
