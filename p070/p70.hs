import Mlib
import Data.List

eulerphi :: Integer -> Integer
eulerphi n = let factors = removeMult $ factorize n in foldr (\x y -> y - y `div` x) n factors

removeMult :: (Integral a) => [a] -> [a]
removeMult [] = []
removeMult (x:xs) = x : (removeMult $ dropWhile (==x) xs)

isPerm :: (Ord a) => [a] -> [a] -> Bool
isPerm x y = sort x == sort y

a70 n = fst $ min $ map (\x -> (x, let phix = eulerphi x in if isPerm (show x) (show $ phix) then (fromIntegral x)/(fromIntegral phix) else 2)) [2..n]
          where min (x:[]) = x
                min ((x1, x2):(y1, y2):xs) = min $ if x2 < y2 then (x1,x2):xs else (y1,y2):xs